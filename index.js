
const FIRST_NAME = "Zaharia";
const LAST_NAME = "Loredana";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() {
var Object={};
var EmptyObject={};

Object.pageAccessCounter=function(n='home')
{
    n=n.toLowerCase();
if(EmptyObject[n]==undefined)
    {
    EmptyObject[n]=1;
    }
else{
    EmptyObject[n]++;
    }
}
Object.getCache=function()
{
   return EmptyObject;
   }
return Object;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

